<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package onted
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="to2k7Aa-7hScjcj13-tXPp4RUYZw6jVtP2d_w4a_Tow" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'onted' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="pre-header">
		</div>
		<nav id="site-navigation" class="top-bar" data-topbar role="navigation">
				<ul class="title-area">

	     		<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
	    	<li class="toggle-topbar"><a href="#"><span>Menu</span></a></li>
	  	</ul>
			<section class="top-bar-section">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'centered' ) ); ?>
			</section>
		</nav><!-- #site-navigation -->

		<div class="site-branding">
			<div class="logo">
				<a href="/"><img src="http://ontariostreet.com/wp-content/uploads/2015/12/cropped-round-logo-no-text_retina_3201.png"/></a>
			</div>
			<div class="blog-title">
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<span class="site-subtitle"><h6 class="site-description"><?php bloginfo( 'description' ); ?></h6></span>
			</div>
		</div><!-- .site-branding -->

	</header><!-- #masthead -->
	<div id="content" class="site-content">
