<?php
/**
 * Template Name: Projects Page.
 *
 * This is the template that displays the contact page.
 *
 * @package Axia
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="projects-row">

					<div class="entry-header">
						<h1 class="entry-title">Projects</h1>
					</div>
					<div class="entry-page-content">
						<?php
							$args = array('post_type' => 'gallery_item',  'posts_per_page'  => 12);
							$wp_query = new WP_Query($args);

						if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
						<div class="small-12 medium-6 large-6 columns zoom">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
							<?php
								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'axia' ),
									'after'  => '</div>',
								) );
							?>
						</div>
						<?php endwhile; else: ?>
							<p>No computable...!! Nothing there.</p>
							<?php wp_reset_postdata(); // reset the query
						endif; ?>
					</div>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
