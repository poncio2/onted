<?php
/**
 * The template for displaying search results pages.
 *
 * @package Axia
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="entry-header">
				<h1 class="entry-title"><?php printf( esc_html__( 'Search Results for: %s', 'axia' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->
			<div class="entry-page-content posting">
				<ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php
							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'blog-page' );
							?>

						<?php endwhile; ?>

						<?php the_posts_navigation(); ?>

					<?php else : ?>

						<?php get_template_part( 'template-parts/content', 'none' ); ?>

					<?php endif; ?>
			</ul><!-- #masonry-container -->
		</div><!-- .entry-page-content -->
		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
