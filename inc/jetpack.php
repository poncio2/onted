<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Axia
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function axia_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'axia_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function axia_jetpack_setup
add_action( 'after_setup_theme', 'axia_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function axia_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function axia_infinite_scroll_render
