<?php
/**
 * Template Name: 00_029
 *
 * The template for displaying the front page.
 *
 * This is the template that displays the custom front page for our site.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package guardian
 */

get_header('front');

    $page = get_page_ID('Home');
    $thumb_id = get_post_thumbnail_id();
    $thumb_url = wp_get_attachment_image_src($thumb_id, 'full');
?>
<div id="container" class="container">
  <header class="intro">

  	<div class="intro__image" style="background-image: url('<?php echo $thumb_url[0]; ?>')" alt="Sessions (00_029)"></div>

  	<div class="intro__content">

  		<h1 class="intro__title">Ontario Street</h1>

  		<div class="intro__subtitle">

  			<div class="codrops-links">
  				<a class="front-logo" href="/" title="Logo"><img src="http://ontariostreet.com/wp-content/uploads/2016/05/round-logo-no-text_retina_320_white.png" alt="logo" /></a>


  			</div>

  			<div class="intro__description"><a href="<?php echo get_post_meta( $page, 'subtitle-link', true); ?>" class="subtitle-link"><?php echo get_post_meta( $page, 'subtitle', true); ?></a></div>

  			<button class="trigger">

            <svg width="100%" height="100%" viewBox="0 0 60 60" preserveAspectRatio="none">
								<g class="icon icon--grid">
									<rect x="32.5" y="5.5" width="22" height="22"/>
									<rect x="4.5" y="5.5" width="22" height="22"/>
									<rect x="32.5" y="33.5" width="22" height="22"/>
									<rect x="4.5" y="33.5" width="22" height="22"/>
								</g>
								<g class="icon icon--cross">
									<line x1="4.5" y1="55.5" x2="54.953" y2="5.046"/>
									<line x1="54.953" y1="55.5" x2="4.5" y2="5.047"/>
								</g>
							</svg>

  				<span>View content</span>

  			</button>

  		</div><!-- /intro__subtitle -->

  	</div><!-- /intro__content -->

  </header><!-- /intro -->


  <section class="items-wrap">
    <div class="player">
      <div class="iframe">
        <?php echo get_post_meta( $page, 'embed', true); ?>
      </div><!-- .iframe -->
    </div><!-- .player -->

      <?php

       echo get_post_meta( $page, 'session-1', true);
       echo get_post_meta( $page, 'session-2', true);
       echo get_post_meta( $page, 'session-3', true);
       echo get_post_meta( $page, 'session-4', true);
       ?>





  </section><!-- /items-wrap -->


</div><!-- /container -->
<?php
get_footer();
