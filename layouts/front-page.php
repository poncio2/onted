<?php
/**
 * Template Name: Front-Page
 *
 * This is the template that displays only the front page.
 *
 * @package onted
 */

get_header(); ?>

<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

	<div id="pic1" class="hero" style="background-image: url('<?php echo $thumb_url[0]; ?>')">

	</div><!-- .hero -->
	<div class="crystal">
	</div><!-- .crystal -->


<section class="content-front">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="front-sessions">
				<header class="entry-header">
					<a href="/sessions"><h1 class="entry-title">Sessions</h1></a>
				</header><!-- .entry-header -->

							<?php
								$args = array('post_type' => 'session',  'posts_per_page'  => 4, 'post_status' => 'publish');
								$wp_query = new WP_Query($args);

								if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div class="small-12 medium-6 large-3 columns session-image-front zoom">

									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
									<?php
										wp_link_pages( array(
											'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'onted' ),
											'after'  => '</div>',
										) );
									?>
								</div>
								<?php endwhile; else: ?>
								<p>No computable!! Nothing there.</p>
								<?php wp_reset_postdata(); // reset the query
								endif; ?>

			</div><!-- .front-sessions -->

			<div class="front-video">

				<div class="small-12 large-9 columns entry-image">

					<?php
					 $page = get_page_ID('Home');
					 echo get_post_meta( $page, 'entry', true);?>

					<div class="videoWrapper">
						<?php echo get_post_meta( $page, 'embed', true);?>
				  </div>
				</div><!-- .entry-image -->
				<div class="small-12 large-3 columns">
					<?php
						$formats = new WP_Query( array(
							'posts_per_page' => 3,
							'tax_query' => array(
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => 'post-format-video',
									'operator' => 'IN',
									)
			 				)
						));

					if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
					<a href="<?php the_permalink(); ?>"><h5 class='entry-title'><?php the_title(); ?></h5></a>
					<div class="videoWrapper">
						<?php echo get_post_meta($post->ID, 'embed', true);?>
					</div>
					<?php endwhile; else: ?>
						<p>No computable...!! Nothing there.</p>
						<?php wp_reset_postdata(); // reset the query
					endif; ?>
				</div>

			</div><!-- .front-page-content -->
			<div class="projects-row front-row last-row">
					<?php
						$args = array('post_type' => 'artwork',  'posts_per_page'  => 4);
						$wp_query = new WP_Query($args);

					if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<div class="small-12 medium-6 large-3 columns zoom">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

						<?php
							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'onted' ),
								'after'  => '</div>',
							) );
						?>
						<div class="entry-header">
							<h4 class="entry-title"><?php the_title(); ?></h4>
						</div><!-- .entry-header -->
					</div>
					<?php endwhile; else: ?>
						<p>No computable...!! Nothing there.</p>
						<?php wp_reset_postdata(); // reset the query
					endif; ?>

			</div><!-- .projects-row -->

			<div class="front-records">

					<div class="small-12 medium-12 large-4 columns ">


						<?php
						$formats = new WP_Query( array(
							'posts_per_page' => 2,
							'tax_query' => array(
								array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => 'sci-fi'
									),
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-status', 'post-format-audio', 'post-format-chat', 'post-format-video'),
									'operator' => 'NOT IN'
								 )
							)
						));

					if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
								<div>
									<header class="article-header marg-bottom"><a href='<?php the_permalink(); ?>' rel="bookmark">
										<?php
											$thumb_id = get_post_thumbnail_id();
											$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

											<div class="article-thumb" style="background-image: url('<?php echo $thumb_url[0]; ?>');">

											</div>


										<h2 class="entry-title">
											<?php the_title(); ?>
										</h2>
									</div><!-- .entry-meta -->
								</a></header><!-- .entry-header -->
							<?php endwhile; else: ?>
							<p>No computable!! Nothing there.</p>
							<?php wp_reset_postdata(); // reset the query
							endif; ?>
						</div>
					<div class="small-12 medium-12 large-4 columns">

						<?php
						$formats = new WP_Query( array(
							'posts_per_page' => 2,
							'tax_query' => array(
								array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => 'music'
									),
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-status', 'post-format-audio', 'post-format-chat', 'post-format-video'),
            			'operator' => 'NOT IN'
								 )
							)
						));

					if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
								<div>
									<header class="article-header marg-bottom"><a href='<?php the_permalink(); ?>' rel="bookmark">
										<?php
											$thumb_id = get_post_thumbnail_id();
											$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

											<div class="article-thumb" style="background-image: url('<?php echo $thumb_url[0]; ?>');">

											</div>


										<h2 class="entry-title">
											<?php the_title(); ?>
										</h2>
										</a></header><!-- .entry-header -->
									</div><!-- .entry-meta -->

							<?php endwhile; else: ?>
							<p>No computable!! Nothing there.</p>
							<?php wp_reset_postdata(); // reset the query
							endif; ?>
						</div>
						<div class="small-12 medium-12 large-4 columns">
							<div class="entry-header">
								<h4 class="entry-title">Out There</h4>
							</div><!-- .entry-header -->
							<?php
								$args = array('post_type' => 'tune',  'posts_per_page'  => 3, 'post_status' => 'publish');
								$wp_query = new WP_Query($args);

								if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div>

										<a href="/tunes"><h5 class="record-title centered"><?php the_title(); ?></h5></a>

									<?php echo get_post_meta($post->ID, 'embed', true); ?>
								</div>
								<?php endwhile; else: ?>
								<p>No computable!! Nothing there.</p>
								<?php wp_reset_postdata(); // reset the query
								endif; ?>
							</div>
			</div><!-- .front-records -->

		</main><!-- #main -->
	</div><!-- #primary -->
</section>

<?php get_footer(); ?>
