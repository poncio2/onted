<?php
/**
 * Template Name: Tunes
 *
 *This is the template that displays the tunes page.
 * @package Axia
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="small-12 medium-12 large-4 columns">


					<?php
					$args = array('post_type' => 'tune',  'posts_per_page'  => 8, 'category_name' => 'cloudmusic', 'post_status' => 'publish');
					$wp_query = new WP_Query($args);

					if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<div>

						<h5 class="record-title centered"><?php the_title(); ?></h5>

						<?php echo get_post_meta($post->ID, 'embed', true); ?>
					</div>
					<?php endwhile; else: ?>
					<p>No computable!! Nothing there.</p>
					<?php wp_reset_postdata(); // reset the query
					endif; ?>
					<?php the_posts_navigation(); ?>
				</div>
			<div class="small-12 medium-12 large-4 columns">

					<?php
					$args = array('post_type' => 'tune',  'posts_per_page'  => 10, 'category_name' => 'Bandcamp', 'post_status' => 'publish');
					$wp_query = new WP_Query($args);

					if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<div>

						<h5 class="record-title centered"><?php the_title(); ?></h5>

						<?php echo get_post_meta($post->ID, 'embed', true); ?>
					</div>
					<?php endwhile; else: ?>
					<p>No computable!! Nothing there.</p>
					<?php wp_reset_postdata(); // reset the query
					endif; ?>
					<?php the_posts_navigation(); ?>
				</div>
				<div class="small-12 medium-12 large-4 columns">

					<?php
					$formats = new WP_Query( array(
						'posts_per_page' => 20,
						'tax_query' => array(
							array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => 'musicvideo'
								),
							array(
								'taxonomy' => 'post_format',
								'field'    => 'slug',
								'terms'    => 'post-format-video',
								'operator' => 'IN',
							 )
						)
					));

				if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
						<div>
							<header class="article-header tunes-pad">
								<div class="videoWrapper">
									<?php echo get_post_meta($post->ID, 'embed', true);?>
								</div>

								<h2 class="entry-title">
									<?php the_title(); ?>
								</h2>
								</header><!-- .entry-header -->
							</div><!-- .entry-meta -->
						<?php endwhile; else: ?>
						<p>No computable!! Nothing there.</p>
						<?php wp_reset_postdata(); // reset the query
						endif; ?>
						<?php the_posts_navigation(); ?>
					</div>
	</div><!-- .front-records -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
