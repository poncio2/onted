<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Axia
 */

?>



	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-container">
			<div class="footer-widget-wrapper">
				<div class="small-12 medium-12 large-4 columns footer-widget">
					<?php dynamic_sidebar( 'sidebar-3' ); ?>
				</div>
				<div class="small-12 medium-12 large-4 columns footer-widget">
					<?php dynamic_sidebar( 'sidebar-4' ); ?>
				</div>
				<div class="small-12 medium-12 large-4 columns footer-widget">
					<?php dynamic_sidebar( 'sidebar-5' ); ?>
				</div>
			</div>
			<div class="site-info">
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'axia' ) ); ?>" target="_blank"><?php printf( esc_html__( 'Proudly powered by %s', 'axia' ), 'WordPress' ); ?></a>
				<span class="sep"> | </span>
				<a href="<?php echo esc_url( __( 'http://icebot.me', 'axia' ) ); ?>" target="_blank"><?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'axia' ), 'Axia', 'ICEBOT' ); ?></a>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->

	</div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script> jQuery(document).foundation(); </script>

<script>
			(function() {
				var container = document.getElementById( 'container' ),
					trigger = container.querySelector( 'button.trigger' );

				function toggleContent() {
					if( classie.has( container, 'container--open' ) ) {
						classie.remove( container, 'container--open' );
						classie.remove( trigger, 'trigger--active' );
						window.addEventListener( 'scroll', noscroll );
					}
					else {
						classie.add( container, 'container--open' );
						classie.add( trigger, 'trigger--active' );
						window.removeEventListener( 'scroll', noscroll );
					}
				}

				function noscroll() {
					window.scrollTo( 0, 0 );
				}

				// reset scrolling position
				document.body.scrollTop = document.documentElement.scrollTop = 0;

				// disable scrolling
				window.addEventListener( 'scroll', noscroll );

				trigger.addEventListener( 'click', toggleContent );

			})();
</script>

</body>
</html>
