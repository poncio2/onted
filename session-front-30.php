<?php
/**
 * Template Name: Front-30
 *
 * The template for displaying the front page.
 *
 * This is the template that displays the custom front page for our site.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package guardian
 */

get_header('front');

    $thumb_id = get_post_thumbnail_id();
    $thumb_url = wp_get_attachment_image_src($thumb_id, 'full');
?>


    <div class="front-menu-wrap">
      <a href="#"><div>
        <i  id="gua-hamburger" class="gua-closed material-icons">menu</i>
      </div></a>
      <ul>
        <a href="/content"><li id="gua-menu1" class="gua-men">Content</li></a>
        <a href="/sessions"><li id="gua-menu2" class="gua-men">Sessions</li></a>
        <a href="/tunes"><li id="gua-menu3" class="gua-men">Tunes</li></a>
        <a href="http://catalog.ontariostreet.com" target="_blank"><li id="gua-menu4" class="gua-men">Catalog</li></a>
        <a href="http://noid.ontariostreet.com" target="_blank"><li id="gua-menu5" class="gua-men">noID</li></a>
      </ul>
      <div id="gua-menu">

      </div>
    </div> <!-- .front-menu-wrap -->


  <div class="screen">
    <div class="title-box">
      <div class="box-30">
        <h1 class="title-30 animated fadeInDown">Ontario Street</h1>
        <a href="/session-28" class="ssn-30"><h3 class="link-ssn-30 animated fadeInRight">Sessions: (00_0031)</h3></a>
      </div>
    </div>
  </div><!-- .screen -->

  <a href="#"><div class="player-28">
    <i class="material-icons grey-color player-play">keyboard_arrow_up</i>
    <iframe width="100%" height="60" src="" frameborder="0"></iframe>
  </div></a><!-- .player-28 -->


<?php
get_footer("front");
