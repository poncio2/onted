<?php
/**
 * The template used for displaying page content in front-page.php
 *
 * @package Axia
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-page-content">


		<div class="entry-content">
			<div class="small-12 large-12 columns entry-image">
				<?php if( has_post_thumbnail() ) {
					the_post_thumbnail();
				}?>
			</div><!-- .entry-image -->
			<div class="small-12 large-12 columns">
				<header class="entry-header">
					<?php the_title('<h1 class="entry-title">', '</h1>'); ?>
				</header><!-- .entry-header -->

				<div class="entry-meta-session">
					<blockquote>
						<ul class="session-meta">
							<li><i class="material-icons">person</i><?php the_author_link(); ?></li>
							<li><i class="material-icons">watch</i><?php the_date(); ?></li>
						</ul>
					</blockquote>
					<blockquote>
						<ul class="session-meta">
							<li><i class="material-icons">folder_special</i><?php the_category( ' | '); ?></li>
						</ul>
					</blockquote>
				</div><!-- .entry-meta -->

				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'axia' ),
						'after'  => '</div>',
					) );
				?>
			</div>

		</div><!-- .entry-content -->
	</div><!-- .entry-page-content -->
</article><!-- #post-## -->
