<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Axia
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="small-12 medium-6 large-4 columns">
		<div class="article-box">
			<header class="article-header"><a href='<?php the_permalink(); ?>' rel="bookmark">
				<?php if( the_post_thumbnail() ) : ?>
					<div class="article-thumb ">
						<?php the_post_thumbnail(); ?>
					</div>
				<?php endif; ?>

				<h2 class="entry-title"><?php the_title(); ?><h2>

				<?php if ( 'post' == get_post_type() ) : ?>
				<div class="entry-meta">
					<?php axia_posted_on(); ?>
				</div><!-- .entry-meta -->
				<?php endif; ?>
			</a></header><!-- .entry-header -->
			<div class="article-content">
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
			</div><!-- .article-content -->

			<footer class="entry-footer">
				<?php axia_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</div><!-- .article-box -->
	</div><!-- .columns -->
</article><!-- #post-## -->
