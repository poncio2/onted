<?php
/**
 * The template used for displaying page content in contact page.
 *
 * @package Axia
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

		<div class="entry-page-content">
			<div class="small-12 medium-12 large-8 columns pad-top ">

						<?php the_post_thumbnail(); ?>

<?php endif; ?>
			</div><!-- .entry-image -->
			<div class="small-12 medium-12 large-4 columns entry-text pad-top">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'axia' ),
						'after'  => '</div>',
					) );
				?>
			</div> <!-- .entry-text -->
		</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'axia' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
