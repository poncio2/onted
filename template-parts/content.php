<?php
/**
 * Template part for displaying posts.
 *
 * @package Axia
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


		<div class="row">
			<div class="small-12 large-12 columns post-image ">
				<?php the_post_thumbnail(); ?>
			</div><!-- post-image-->
		</div><!-- row-->

		<div class="entry-content">

			<div class="standard-post-content">
				<header class="entry-header">
					<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>

				</header><!-- .entry-header -->
				<div class="small-12 medium-8 large-8 columns pad-bottom">
					<div class="entry-meta-session">
						<blockquote>
							<ul class="session-meta">
								<li><i class="material-icons">person</i><?php the_author_link(); ?></li>
								<li><i class="material-icons">watch</i><?php the_date(); ?></li>
							</ul>
						</blockquote>
						<blockquote>
							<ul class="session-meta">
								<li><i class="material-icons">folder_special</i><?php the_category( ' | '); ?></li>
							</ul>
						</blockquote>
					</div><!-- .entry-meta -->

					<?php
						the_content( sprintf(
							/* translators: %s: Name of current post. */
							wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'axia' ), array( 'span' => array( 'class' => array() ) ) ),
							the_title( '<span class="screen-reader-text">"', '"</span>', false )
						) );
					?>
					<?php the_tags('<blockquote class="onted-tag"><i class="material-icons tag-icon">label</i>  ', ' | ', '</blockquote>') ?>
					<?php the_post_navigation(); ?>
					<?php
						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'axia' ),
							'after'  => '</div>',
						) );
					?>
					<div class="onted-comments">
						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
					</div><!-- .onted-comments -->
			</div>

			<div class="small-12 medium-4 large-4 columns">
				<?php get_sidebar(); ?>
			</div>
		</div><!-- .standard-post-content -->
	</div><!-- .entry-content -->


</article><!-- #post-## -->
