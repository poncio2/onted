<?php
/**
 * The template part for displaying results in blog pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Axia
 */

?>

		<li>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>  >
				<div class="article-box">

						<header class="article-header"><a href='<?php the_permalink(); ?>' rel="bookmark">
							<?php
								$thumb_id = get_post_thumbnail_id();
								$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

								<div class="article-thumb" style="background-image: url('<?php echo $thumb_url[0]; ?>');">

								</div>


							<h2 class="entry-title blog-entry-title">
								<?php the_title(); ?>
							</h2>


						</header></a><!-- .entry-header -->


						<div class="article-content">

							<div class="entry-meta">
								<?php axia_posted_on(); ?>
							</div><!-- .entry-meta -->

							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div><!-- .entry-summary -->
						</div><!-- .article-content -->

						<footer class="entry-footer">
							<?php axia_entry_footer(); ?>
						</footer><!-- .entry-footer -->

				</div><!-- .article-box -->
			</article><!-- #post-## -->
		</li>
