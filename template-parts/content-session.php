<?php
/**
 * The template used for displaying sessions content in single page.
 *
 * @package Axia
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

		<div class="entry-page-content content-session">
			<div class="small-12 medium-12 large-8 columns pad-top ">
						<?php the_post_thumbnail(); ?>
			</div>
			<div class="small-12 medium-12 large-4 columns entry-text pad-top single-content">

					<div class="entry-meta-session">
            <blockquote>
  						<ul class="session-meta">
                <li><i class="material-icons">person</i><?php the_author_link(); ?></li>
                <li><i class="material-icons">watch</i><?php the_date(); ?></li>
              </ul>
            </blockquote>
            <blockquote>
              <ul class="session-meta">
                <li><i class="material-icons">folder_special</i><?php the_category( ' | '); ?></li>
              </ul>
            </blockquote>
          </div><!-- .entry-meta -->
          <div class="vertical-bottom">
  					<?php the_content(); ?>
  					<?php
  						wp_link_pages( array(
  							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'axia' ),
  							'after'  => '</div>',
  						) );
  					?>
  					<?php the_tags('<blockquote class="onted-tag"><i class="material-icons tag-icon">label</i>  ', ' | ', '</blockquote>') ?>
  					<?php the_post_navigation(); ?>
				  </div>
			</div> <!-- .entry-text -->
		</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'axia' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
