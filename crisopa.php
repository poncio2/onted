<?php
/**
 * Template Name: Crisopa
 *
 * This is the template that displays only the front page with Crisopa event.
 *
 * @package onted
 */

get_header(); ?>


<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

	<div id="pic1" class="hero" style="background-image: url('<?php echo $thumb_url[0]; ?>')">

		<div class="yellow-frame"></div>

	</div><!-- .hero -->
	<div class="crystal">

		<div class="portada">
			<div class="frame-1">
				<div class="small-12 medium-6 large-6 columns" id="crisopa">
					<div class="text-table">
						<div class="text-cell">
							<div class="ev-text" id="evento-name">ONT.ST/00_01</div>
							<h3 class="ev-title"><a href="http://ontariostreet.com/blog/2016/02/18/crisopa-live-ont-st00_01/">CRISOPA</a><span class="ev-text">  LIVE</span></h3>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 large-6 columns" id="horario">
					<div class="text-table">
						<div class="text-cell">
							<p class="ev-text text-bottom">HORARIO: APERTURA 12:00H / 13:30H</p>
						</div>
					</div>
				</div>
			</div>
			<div class="frame-2">
				<div class="small-12 medium-6 large-6 columns cell-a">
					<div class="ev-text"><a href="http://n5md.com/" target="_blank">N5MD</a> - PER SONA I SLA</div>
					<div class="ev-text"><a href="https://soundcloud.com/crisopa" target="_blank">soundcloud.com/crisopa</a></div>
				</div>
				<div class="small-12 medium-6 large-6 columns cell-b">
					<h3 class="ev-title">11.03.2016  \\</h3>
				</div>
			</div>
			<div class="frame-3">
				<div class="small-12 medium-6 large-6 columns logos-evento">
					<div class="text-table">
						<div class="text-cell">
							<div class="text-tradas">AFORO LIMITADO 25P ( SOLO VENTA ANTICIPADA )</div>
							<div  class="text-tradas">ENTRADAS:</div>
							<div class="entradas text-tradas">OH DELICE! - Maldonado 44 - Madrid</div>
							<div class="entradas text-tradas">EL ALMACEN - Minas 13 - Madrid</div>
							<div class="entradas text-tradas">ONTARIO STREET - 606620962 - info@ontariostreet.com</div>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 large-6 columns">
					<div class="text-table">
						<div class="text-cell">
							<ul class="small-block-grid-2 logos">
								<li id="logo-1"><a href="https://www.facebook.com/elalmacendediscos/" target="_blank"><img src="http://ontariostreet.com/wp-content/uploads/2016/02/El_almacen_logo.png" /></a></li>
								<li id="logo-2"><a href="http://www.ohdelice.es/donde-estamos/" target="_blank"><img src="http://ontariostreet.com/wp-content/uploads/2016/02/oh_delice.png" /></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div><!-- .crystal -->


<section class="content-front">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="front-sessions">
				<header class="entry-header">
					<a href="/sessions"><h1 class="entry-title">Sessions</h1></a>
				</header><!-- .entry-header -->

							<?php
								$args = array('post_type' => 'session',  'posts_per_page'  => 4, 'post_status' => 'publish');
								$wp_query = new WP_Query($args);

								if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div class="small-12 medium-6 large-3 columns session-image-front zoom">

									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
									<?php
										wp_link_pages( array(
											'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'onted' ),
											'after'  => '</div>',
										) );
									?>
								</div>
								<?php endwhile; else: ?>
								<p>No computable!! Nothing there.</p>
								<?php wp_reset_postdata(); // reset the query
								endif; ?>

			</div><!-- .front-sessions -->

			<div class="front-video">

				<div class="small-12 large-9 columns entry-image">

					<?php
					 $page = get_page_ID('Home');
					 echo get_post_meta( $page, 'entry', true);?>

					<div class="videoWrapper">
						<?php echo get_post_meta( $page, 'embed', true);?>
				  </div>
				</div><!-- .entry-image -->
				<div class="small-12 large-3 columns">
					<?php
						$formats = new WP_Query( array(
							'posts_per_page' => 3,
							'tax_query' => array(
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => 'post-format-video',
									'operator' => 'IN',
									)
			 				)
						));

					if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
					<a href="<?php the_permalink(); ?>"><h5 class='entry-title'><?php the_title(); ?></h5></a>
					<div class="videoWrapper">
						<?php echo get_post_meta($post->ID, 'embed', true);?>
					</div>
					<?php endwhile; else: ?>
						<p>No computable...!! Nothing there.</p>
						<?php wp_reset_postdata(); // reset the query
					endif; ?>
				</div>

			</div><!-- .front-video -->

			<div class="front-records">

					<div class="small-12 medium-12 large-4 columns ">


						<?php
						$formats = new WP_Query( array(
							'posts_per_page' => 2,
							'tax_query' => array(
								array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => 'sci-fi'
									),
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-status', 'post-format-audio', 'post-format-chat'),
									'operator' => 'NOT IN'
								 )
							)
						));

					if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
								<div>
									<header class="article-header marg-bottom"><a href='<?php the_permalink(); ?>' rel="bookmark">
										<?php
											$thumb_id = get_post_thumbnail_id();
											$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

											<div class="article-thumb" style="background-image: url('<?php echo $thumb_url[0]; ?>');">

											</div>


										<h2 class="entry-title">
											<?php the_title(); ?>
										</h2>
									</div><!-- .entry-meta -->
								</a></header><!-- .entry-header -->
							<?php endwhile; else: ?>
							<p>No computable!! Nothing there.</p>
							<?php wp_reset_postdata(); // reset the query
							endif; ?>
						</div>
					<div class="small-12 medium-12 large-4 columns">

						<?php
						$formats = new WP_Query( array(
							'posts_per_page' => 2,
							'tax_query' => array(
								array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => 'music'
									),
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-status', 'post-format-audio', 'post-format-chat'),
            			'operator' => 'NOT IN'
								 )
							)
						));

					if($formats->have_posts()) : while($formats->have_posts()) : $formats->the_post();?>
								<div>
									<header class="article-header marg-bottom"><a href='<?php the_permalink(); ?>' rel="bookmark">
										<?php
											$thumb_id = get_post_thumbnail_id();
											$thumb_url = wp_get_attachment_image_src($thumb_id, 'full'); ?>

											<div class="article-thumb" style="background-image: url('<?php echo $thumb_url[0]; ?>');">

											</div>


										<h2 class="entry-title">
											<?php the_title(); ?>
										</h2>
										</a></header><!-- .entry-header -->
									</div><!-- .entry-meta -->

							<?php endwhile; else: ?>
							<p>No computable!! Nothing there.</p>
							<?php wp_reset_postdata(); // reset the query
							endif; ?>
						</div>
						<div class="small-12 medium-12 large-4 columns">
							<div class="entry-header">
								<h4 class="entry-title">Out There</h4>
							</div><!-- .entry-header -->
							<?php
								$args = array('post_type' => 'tune',  'posts_per_page'  => 4, 'post_status' => 'publish');
								$wp_query = new WP_Query($args);

								if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div>

										<a href="/tunes"><h5 class="record-title centered"><?php the_title(); ?></h5></a>

									<?php echo get_post_meta($post->ID, 'embed', true); ?>
								</div>
								<?php endwhile; else: ?>
								<p>No computable!! Nothing there.</p>
								<?php wp_reset_postdata(); // reset the query
								endif; ?>
							</div>
			</div><!-- .front-records -->

		</main><!-- #main -->
	</div><!-- #primary -->
</section>

<?php get_footer(); ?>
