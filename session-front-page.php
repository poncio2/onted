<?php
/**
 * Template Name: (00_028)
 *
 * This is the template that displays only the front page for Session 00_028.
 *
 * @package onted
 */
 ?>
<!DOCTYPE html>
 <html <?php language_attributes(); ?>>
 <head>
 <meta charset="<?php bloginfo( 'charset' ); ?>">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="google-site-verification" content="to2k7Aa-7hScjcj13-tXPp4RUYZw6jVtP2d_w4a_Tow" />
 <link rel="profile" href="http://gmpg.org/xfn/11">
 <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


 <?php wp_head(); ?>
 </head>

 <body <?php body_class(); ?>>
 <div id="page" class="hfeed site">
 	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'onted' ); ?></a>

 	<header>

 	</header><!-- #masthead -->
 	  <div id="content" class="site-content">

      <?php
      	$thumb_id = get_post_thumbnail_id();
      	$thumb_url = wp_get_attachment_image_src($thumb_id, 'full');
      ?>

  		<div id="pic1" class="hero" style="background-image: url('<?php echo $thumb_url[0]; ?>')"></div><!-- .hero -->

      <div class="crystal">
        <nav id="site-navigation" class="top-bar top-bar-28" data-topbar role="navigation">
            <ul class="title-area">

              <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar"><a href="#"><span class="menu-mobile-28">Menu</span></a></li>
          </ul>
          <section class="top-bar-section">
            <ul>
              <li><a href="/content" class="li-menu-28 after-28">Content</a></li>
              <li><a href="/sessions" class="li-menu-28 after-28">Sessions</a></li>
              <li><a href="/tunes" class="li-menu-28 after-28">Tunes</a></li>
              <li><a href="http://catalog.ontariostreet.com" class="li-menu-28 after-28">Catalog</a></li>
              <li><a href="http://noid.ontariostreet.com" class="li-menu-28">noID</a></li>
            </ul>
          </section>
        </nav><!-- #site-navigation -->
    		  <h1 class="titulo-28"><a href="/">Ontario Street</a></h1>
          <h3 class="session-28"><a href="/blog/sessions/sessions-00_028">Sessions: (00_028)</a></h3>

          <div class="menu-28">
            <ul>
              <li><a href="/content" class="li-menu-28 after-28">Content</a></li>
              <li><a href="/sessions" class="li-menu-28 after-28">Sessions</a></li>
              <li><a href="/tunes" class="li-menu-28 after-28">Tunes</a></li>
              <li><a href="http://catalog.ontariostreet.com" class="li-menu-28 after-28">Catalog</a></li>
              <li><a href="http://noid.ontariostreet.com" class="li-menu-28">noID</a></li>
            </ul>
          </div>

          <a href="#"><div class="player-28">
            <i class="material-icons grey-color player-play">keyboard_arrow_up</i>
            <i class="material-icons play-text">play_circle_filled</i>
            <iframe width="100%" height="60" src="https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2Fontariostreet%2F00_028%2F&hide_cover=1&mini=1&light=1" frameborder="0"></iframe>
          </div></a>
    	</div><!-- .crystal -->


    </div><!-- .content -->
  </div><!-- .page -->
  <?php wp_footer(); ?>

  <script> jQuery(document).foundation(); </script>
  <script> jQuery(document).ready(function() {
    jQuery(".player-28").on('click', function() {
      if ($(".player-28").hasClass("active")) {
        jQuery(".player-play").text('keyboard_arrow_up');
        jQuery(".play-text").css("display", "block");
        jQuery(".player-28").removeClass('active').css('bottom', '-70px');

      } else {
        jQuery(".player-play").text('keyboard_arrow_down');
        jQuery(".play-text").css("display", "none");
        jQuery(".player-28").addClass('active').css('bottom', '0');
      };

    });

  });
  </script>
  </body>
</html>
