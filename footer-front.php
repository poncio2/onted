<?php
/**
 * The template for the front page custom footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package onted
 */

?>

	</div><!-- #content -->

</div><!-- #page -->

<?php wp_footer(); ?>
<script>
	$(document).ready(function(){
		$("#gua-hamburger").on("click", function() {
			if($("#gua-hamburger").hasClass("gua-closed")) {
				$("#gua-hamburger").text("keyboard_arrow_up").removeClass("gua-closed").addClass("gua-open");
				$("#gua-menu").fadeIn(500);
				$(".gua-men").each(function(index) {
					$(this).delay(120*index).fadeIn(300);
				});
			} else {
				$("#gua-hamburger").text("menu").removeClass("gua-open").addClass("gua-closed");
				$("#gua-menu").fadeOut(500);
				$($(".gua-men").get().reverse()).each(function(index) {
					$(this).delay(120*index).fadeOut(300);
				});
			}
		})
		$("#gua-menu").on("click", function() {
 		if( $("#gua-hamburger").hasClass("gua-open")) {
			 $("#gua-hamburger").text("menu").removeClass("gua-open").addClass("gua-closed");
			 $("#gua-menu").fadeOut(500);
			 $($(".gua-men").get().reverse()).each(function(index) {
				 $(this).delay(120*index).fadeOut(300);
			 });
		 	}
		})
	});
 jQuery(document).ready(function() {
			jQuery(".player-28 iframe").attr('src', 'https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2Fontariostreet%2F00_032%2F&hide_cover=1&mini=1&light=1');
			jQuery(".player-28").delay(1800).addClass('active').animate({
    								bottom: "-8",
									}, 800);

});
</script>
</body>
</html>
