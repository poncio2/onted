<?php
/**
 * Template Name: Sessions
 *
 * Template for sessions Isotope Gallery page.
 *
 * @package Axia
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="sessions-main">
				<div class="selector-list">
					<nav class="iso-nav">
						<ul id="sessions-iso-nav">
							<li>
								<a id="bystyle" href="#">By Style    <i class="material-icons">keyboard_arrow_down</i></a>
								<ul id="filters">
									<li><a href="#" data-filter="*" class="selected iso-button">All</a></li>
									<li><a href="#" data-filter=".idm" class="iso-button">IDM</a></li>
									<li><a href="#" data-filter=".post-rock" class="iso-button">Post Rock</a></li>
									<li><a href="#" data-filter=".ambient" class="iso-button">Ambient</a></li>
									<li><a href="#" data-filter=".experimental" class="iso-button">Experimental</a></li>
								</ul>
							</li>
						<ul>
					</nav><!-- .sessions-iso-nav -->
				</div> <!-- .seslector-list -->
				<div class="sessions-gallery">

							<?php $args = array( 'post_type' => 'session', 'posts_per_page' => -1) ?>
							<?php $the_query = new WP_Query( $args ); //Check the WP_Query docs to see how you can limit which posts to display ?>
							<?php if ( $the_query->have_posts() ) : ?>
							    <div id="isotope-list">



							    <?php while ( $the_query->have_posts() ) : $the_query->the_post();
										$termsArray = wp_get_post_terms( $the_query ->post->ID, "category" );  //Get the terms for this particular item
										$termsString = ""; //initialize the string that will contain the terms
											foreach ( $termsArray as $term ) { // for each term
												$termsString .= $term->slug.' '; //create a string that has all the slugs
											}
										?>
										<a href="<?php the_permalink(); ?>"><div class="small-12 medium-6 large-3 columns <?php echo $termsString; ?> session-item isotope-item zoom">
										        <?php if ( has_post_thumbnail() ) {
									                      the_post_thumbnail();
									                } ?>

										</div></a> <!-- end item -->
							    <?php endwhile;  ?>
							    </div> <!-- end isotope-list -->
							<?php endif; ?>

				</div><!-- .sessions-gallery -->


			</div><!-- .sessions-main -->
		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
