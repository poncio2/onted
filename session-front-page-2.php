<?php
/**
 * Template Name: Front
 *
 * The template for displaying the front page.
 *
 * This is the template that displays the custom front page for our site.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package onted
 */

get_header('front');

    $thumb_id = get_post_thumbnail_id();
    $thumb_url = wp_get_attachment_image_src($thumb_id, 'full');
  ?>

  <div class="front-menu-wrap">
    <a href="#"><div>
    <i  id="gua-hamburger" class="gua-closed material-icons">menu</i>
    </div></a>
    <ul>
      <a href="/blog"><li id="gua-menu1" class="gua-men">Content</li></a>
      <a href="/sessions"><li id="gua-menu2" class="gua-men">Sessions</li></a>
      <a href="/tunes"><li id="gua-menu3" class="gua-men">Tunes</li></a>
      <a href="http://catalog.ontariostreet.com" target="_blank"><li id="gua-menu4" class="gua-men">Catalog</li></a>
      <a href="http://noid.ontariostreet.com" target="_blank"><li id="gua-menu5" class="gua-men">noID</li></a>
    </ul>
    <div id="gua-menu">

    </div>
  </div> <!-- .front-menu-wrap -->
  <div class="hero row" style="background-image: url('<?php echo $thumb_url[0]; ?>')">

    <div class="void-left small-12 medium-6 large-9 columns">

      <h1 class="front-title">ONTARIO STREET</h1>
      <a href="/sessions_00_029"><h2 class="front-sub-title">Sessions: (00_029)</h2></a>

    </div> <!-- .void-left -->
    <div class="title-block small-12 medium-6 large-3 columns">

    </div><!-- .title-block -->


    <a href="#"><div class="player-28">
              <i class="material-icons grey-color player-play">keyboard_arrow_up</i>
              <i class="material-icons play-text">play_circle_filled</i>
              <iframe width="100%" height="60" src="https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2Fontariostreet%2F00_028%2F&hide_cover=1&mini=1&light=1" frameborder="0"></iframe>
            </div></a>
  </div><!-- .hero -->



  <?php
  get_footer('front');
