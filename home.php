<?php
/**
 * The template for displaying blog pages.
 *
 * @package Axia
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="entry-page-content posting">
				<ul id="masonry-container" class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
					<?php /* Start the Loop */ ?>
					<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php
						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'blog-page' );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>
				<div class="blog-post-navigation">
					<?php the_posts_navigation(); ?>
				</div><!-- .post-navigation -->
			</ul><!-- #masonry-container -->
		</div><!-- .entry-page-content -->
		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
